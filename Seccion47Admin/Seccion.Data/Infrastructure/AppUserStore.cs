﻿using Seccion.Model.IdentityModel;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Seccion.Data.Infrastructure
{
    public class AppUserStore: UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUserStore<ApplicationUser, int>
    {

        public AppUserStore(StoreEntities MyDbContext): base(MyDbContext)
        {
        }
        public override async Task CreateAsync(ApplicationUser appuser)

        {
            await base.CreateAsync(appuser);
            await AddToUsedPasswordAsync(appuser, appuser.PasswordHash);
        }

        public Task AddToUsedPasswordAsync(ApplicationUser appuser, string userpassword)
        {
            appuser.UserUsedPassword.Add(new UsedPassword() { Id = appuser.Id, HashPassword = userpassword });
            return UpdateAsync(appuser);
        }

    }
}
