﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class ExpensesViewModel
	{
        public int TbFuneralExpensesId { get; set; }
        [Display(Name = "Empleado Solicitante")]
        public int TbEmployeeId { get; set; }
        [Display(Name = "No Prestamo")]
        public string OrderNumberGF { get; set; }
        [Display(Name = "Fecha de Creación")]
        public string DateCreate { get; set; }
        [Display(Name = "Concepto Servicio")]
        public string ServiceConcept { get; set; }
        [Display(Name = "Tipo de Pago")]
        public string PaymentType { get; set; }
        [Display(Name = "Clave Voucher")]
        public string VoucherKey { get; set; }
        [Display(Name = "Descontar a:")]
        public string AvalName { get; set; }
        [Display(Name = "Total a Pagar")]
        public decimal PaymentAmount { get; set; }
        [Display(Name = "Periodo de Pagos")]
        public string PeriodsPayment { get; set; }
        [Display(Name = "Abono Catorcenal/Mensual")]
        public decimal AbonoCatorcenalMensual { get; set; }
        [Display(Name = "Observaciones:")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
    }
}