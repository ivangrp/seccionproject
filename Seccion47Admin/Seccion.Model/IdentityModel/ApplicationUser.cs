﻿using Seccion.Model.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Seccion.Model.IdentityModel
{
    
    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {

        public ApplicationUser()
            : base()
        {
            UserUsedPassword = new List<UsedPassword>();
        }
        public string Name { get; set; }
        //Apellido Paterno
        public string FirstName { get; set; }
        //Apellido Materno
        public string LastName { get; set; }
        public string UserPhoto { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool Active { get; set; }
        public bool HasChangedPassword { get; set; }
        public DateTime? JoinDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }

        public virtual List<UsedPassword> UserUsedPassword { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            //Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            //Add custom user claims here
            userIdentity.AddClaim(new Claim("Nombre", this.Name.ToString()));
            //userIdentity.AddClaim(new Claim("Locacion", LocationID.ToString()));
            //userIdentity.AddClaim(new Claim("UserTypeID", UserTypeID.ToString()));
            //userIdentity.AddClaim(new Claim("RankUser", Rank.Description));
            //userIdentity.AddClaim(new Claim("RankID", Rank.RankID.ToString()));
            //userIdentity.AddClaim(new Claim("RankGroupUser", Rank.RankGroup.Description));
            //var Rol = manager.GetRoles(this.Id).FirstOrDefault();
            //userIdentity.AddClaim(new Claim("Rol", Rol));

            return userIdentity;
        }

    }

    public class UsedPassword
    {
        public UsedPassword()
        {
            CreatedDate = DateTime.Now;
        }

        [Key, Column(Order = 0)]
        public string HashPassword { get; set; }
        public DateTime CreatedDate { get; set; }
        [Key, Column(Order = 1)]
        public int Id { get; set; }
        [ScriptIgnore]
        public virtual ApplicationUser AppUser { get; set; }

    }
}
