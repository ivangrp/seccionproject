﻿using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Configuration
{
    public class MenuPermissionConfiguration : BaseEntityTypeConfiguration<MenuPermission>
    {
        public MenuPermissionConfiguration()
        {
            ToTable(nameof(MenuPermission));
            //base.Configure();
        }
    }
}
