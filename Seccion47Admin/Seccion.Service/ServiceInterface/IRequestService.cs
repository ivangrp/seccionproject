﻿using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
	public interface IRequestService
	{
		Task<IEnumerable<SP_RequestDto>> GetListRequest(params object[] parameters);
		Task<TbRequest> GetRequestById(int id);
		void SaveRequest(TbRequest model);
		void UpdateRequest(TbRequest model);
		string GetMaxOrderNumber();
		Task Save();
	}
}
