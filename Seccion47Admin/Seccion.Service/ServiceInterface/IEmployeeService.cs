﻿using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
    public interface IEmployeeService
	{
        Task<IEnumerable<SP_EmployeeDto>> GetEmployeeAll(params object[] parameters);
        Task<IEnumerable<TbEmployee>> GetEmployeeAllForRequest();
        Task<IEnumerable<EmployeeDto>> GetEmployeeData();
        Task<EmployeeDto> GetEmployeeByFicha(string numberFicha);
        Task<TbEmployee> GetEmployeeById(int? id);
        void SaveEmployeeData(TbEmployee employee);
        void UpdateEmployeeData(TbEmployee employee);
        Task<IEnumerable<TbStates>> GetStatesAll();
        Task<IEnumerable<TbMunicipalities>> GetMunicipalitiesAll(int stateId);
        Task Save();
    }
}
