﻿using AutoMapper;
using Seccion.Model.IdentityModel;
using Seccion.Model.Models;
using Seccion.Web.Common;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Seccion.Web.Mappings
{
    /// <summary>
    /// Creates a mapping between source (ViewModel) and destination (Domain)
    /// </summary>
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {

            CreateMap<ListAccountUsersViewModel, ApplicationUser>();
            CreateMap<AccountUserViewModel, ApplicationUser>();
            CreateMap<MyDataViewModel, ApplicationUser> ();
            CreateMap<EmployeeVIewModel, TbEmployee>();
            CreateMap<RequestViewModel, TbRequest>()
                //.ForMember(dest => dest.Amount, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(src.Amount.Replace("$", "").Replace(",", "")), 2)))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.Amount)), 2)))
                .ForMember(dest => dest.AbonoCatorcenalMensual, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.AbonoCatorcenalMensual)), 2)))
                .ForMember(dest => dest.AdministrationExpensesResult, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal( src.AdministrationExpensesResult)),2)))
                .ForMember(dest => dest.TotalPay, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.TotalPay)),2)))
                .ForMember(dest => dest.DiscountRate, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.DiscountRate))))
                .ForMember(dest => dest.DebitAval, opt => opt.MapFrom(src => src.DebitAval != null ? Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.DebitAval)) : 0))
                .ForMember(dest => dest.AdministrationExpenses, opt => opt.MapFrom(src => Convert.ToDecimal(SeparateDecimal.CleanDecimal(src.AdministrationExpenses))))
                .ForMember(dest => dest.DateCreateRequest, opt => opt.MapFrom(src => Convert.ToDateTime(src.DateCreateRequest)));
            CreateMap<ExpensesViewModel, TbFuneralExpenses>();
        }
    }
}