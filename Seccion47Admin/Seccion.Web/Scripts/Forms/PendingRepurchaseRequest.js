﻿$(document).ready(function () {
    GetAllPendingRepurchaseRequest();
});

function GetAllPendingRepurchaseRequest() {
    ComplementDataTable();
    $('#tblRepurchaseRequest').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        filter: true,
        destroy: true,
        pageLength: 25,
        search: {
            "caseInsensitive": false
        },
        "ajax": {
            url: "/" + lang + "/VehicularRequest/GetAllPendingRepurchaseRequest",
            type: "POST",
            datatype: "json",
            error: function (jqXHR, textStatus, error) {
                NotifierMessage("error", "Error!!!", "Ocurrio un problema: " + error + " Estatus: " + jqXHR.status + ". Reportelo con el administrador del sistema por favor.");
            }
        },
        columns: [
            { data: "OrderNumber", name: "OrderNumber", autoWidth: true },
            { data: "TcRequestTypes.Description", name: "TcRequestTypes.Description", autoWidth: true },
            { data: "ApplicationUser.Name", name: "ApplicationUser.Name", autoWidth: true },
            { data: "ApplicationUser.Rank.Description", name: "ApplicationUser.Rank.Description", autoWidth: true },
            { data: "DescriptionVehicle", name: "DescriptionVehicle", autoWidth: true },
            { data: "Chassis", name: "Chassis", autoWidth: true },
            { data: "Plate", name: "Plate", autoWidth: true },
            { data: "Year", name: "Year", autoWidth: true },
            { data: "CarYear", name: "CarYear", autoWidth: true },
            { data: "TcStatus.Description", name: "TcStatus.Description", autoWidth: true },
            { data: "TcTypeCompanie.Description", name: "TcTypeCompanie.Description", autoWidth: true },
            {
                data: null,
                autoWidth: true,
                orderable: false,
                render: function (data, type, full) {
                    var menu = "";
                    menu += "<div class='row'>";
                    menu += "<div class='col-md-12 text-center'>";
                    menu += "<div class='list-icons'>";
                    menu += "<div class='dropdown'>";
                    menu += "<a href='#' class='list-icons-item' data-toggle='dropdown'> <i class='icon-menu9'></i>";
                    menu += "</a>";
                    menu += "<div class='dropdown-menu dropdown-menu-right'>";
                    menu += "<a class='dropdown-item' href='#' onclick='ApprovaRequest(\"" + full.RepurcharseRequestId + "\",\"" + full.RequestTypeId + "\",\"" + full.TcStatus.StepStatus + "\")'><i class='icon-thumbs-up2'></i> Aceitar</a>";
                    menu += "<a class='dropdown-item' href='#' onclick='CancelRequest(\"" + full.RepurcharseRequestId + "\",\"" + full.RequestTypeId + "\")'><i class='icon-thumbs-down2'></i> Reprovar</a>";
                    menu += "</div>";
                    menu += "</div>";
                    menu += "</div>";
                    menu += "</div>";
                    menu += "</div>";
                    return menu;
                }
            }
        ],
        initComplete: function () {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                    .html('<i class="icon-search4"></i>')
                    .click(function () {
                        self.search(input.val()).draw();
                    })
                    .addClass('btn bg-color-nissan btn-icon rounded-round legitRipple');

            $('.dataTables_filter').append($searchButton);
        }
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });
}

function ApprovaRequest(RepurchaseRequestId, RequestTypeId, Step) {
    swal({
        title: '¿Você tem certeza?',
        text: "¿Você quer aprovar o aplicativo selecionado?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#c3092e',
        cancelButtonColor: '#000000',
        allowOutsideClick: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            var json = { "RepurchaseRequestId": RepurchaseRequestId, "RequestTypeId": RequestTypeId, "Step": Step };
            $.ajax({
                url: "/" + lang + "/VehicularRequest/ApprovaRepurchaseRequest",
                type: "POST",
                data: JSON.stringify(json),
                contentType: "application/json; charset:utf-8",
                dataType: 'json'
            }).done(function (data) {
                if (data.success) {
                    GetAllPendingRepurchaseRequest();
                    swal(
                        data.title,
                        data.message,
                        'success'
                    );
                }
                else {
                    GetAllPendingRepurchaseRequest();
                    swal(
                        data.title,
                        data.message,
                        'warning'
                    );
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                swal('Error!!!', "Ocurrio un problema: " + textStatus + ': ' + jqXHR.status + ' .Reportelo con el administrador del sistema por favor. ', 'error');
            });
        }
    });
}

function CancelRequest(RepurchaseRequestId, RequestTypeId) {
    swal({
        title: '¿Você tem certeza?',
        text: "¿Você quer cancelar a aplicação seleccionada?",
        input: 'textarea',
        inputPlaceholder: 'Digite seu comentário.',
        inputClass: 'form-control',
        inputAttributes: {
            maxlength: 200
        },
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#c3092e',
        cancelButtonColor: '#000000',
        allowOutsideClick: false,
        inputValidator: function (value) {
            return !value && 'Este campo é obrigatório';
        }
    }).then(function (result) {
        if (result.value) {
            var json = { "RepurchaseRequestId": RepurchaseRequestId, "RequestTypeId": RequestTypeId, "comentarios": result.value };
            $.ajax({
                url: "/" + lang + "/VehicularRequest/CancelRepurchaseRequet",
                type: "POST",
                data: JSON.stringify(json),
                contentType: "application/json; charset:utf-8",
                dataType: 'json'
            }).done(function (data) {
                if (data.success) {
                    GetAllPendingRepurchaseRequest();
                    swal(
                        data.title,
                        data.message,
                        'success'
                    );
                }
                else {
                    GetAllPendingRepurchaseRequest();
                    swal(
                        data.title,
                        data.message,
                        'warning'
                    );
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                swal('Error!!!', "Ocurrio un problema: " + textStatus + ': ' + jqXHR.status + ' .Reportelo con el administrador del sistema por favor. ', 'error');
            });
        }
    });
}