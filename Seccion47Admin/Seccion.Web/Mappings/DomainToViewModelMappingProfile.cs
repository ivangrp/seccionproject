﻿using AutoMapper;
using Seccion.Data.ResposeDto;
using Seccion.Model.IdentityModel;
using Seccion.Model.Models;
using Seccion.Web.Common;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System;

namespace Seccion.Web.Mappings
{

    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            ConfigureMappings();
        }


        /// <summary>
        /// Creates a mapping between source (Domain) and destination (ViewModel)
        /// </summary>
        private void ConfigureMappings()
        {
            CreateMap<ApplicationUser, ListAccountUsersViewModel>()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<ApplicationUser, AccountUserViewModel> ()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<ApplicationUser, MyDataViewModel>()
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate == null ? "" : src.BirthDate.Value.ToString("yyyy-MM-dd")));
            CreateMap<TbEmployee, EmployeeVIewModel> ();
            CreateMap<SP_EmployeeDto, ListEmployeeViewModel>();
            CreateMap<TbRequest, RequestViewModel>()
                .ForMember(dest => dest.DateCreateRequest, opt => opt.MapFrom(src => src.DateCreateRequest == null ? "" : src.DateCreateRequest.ToString()))
                .ForMember(dest => dest.AbonoCatorcenalMensual, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.AbonoCatorcenalMensual.ToString())))
                .ForMember(dest => dest.AdministrationExpensesResult, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.AdministrationExpensesResult.ToString())))
                .ForMember(dest => dest.TotalPay, opt => opt.MapFrom(src => SeparateDecimal.CleanDecimalFromDataBase(src.TotalPay.ToString())));
        }
    }
}