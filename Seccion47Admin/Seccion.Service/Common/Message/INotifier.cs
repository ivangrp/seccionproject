﻿using System.Collections.Generic;

namespace Seccion.Service.Common.Message
{
    public interface INotifier
    {
        IList<Message> Messages { get; set; }
        void AddMessage(MessageSeverity severity,string title, string text, params object[] format);

    }
}
