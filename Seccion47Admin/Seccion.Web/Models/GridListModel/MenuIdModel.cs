﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.GridListModel
{
	public class MenuIdModel
	{
		public int Id { get; set; }
		public string EditLink { get; set; }
		public string DetailLink { get; set; }
		public string Delete { get; set; }

	}
}