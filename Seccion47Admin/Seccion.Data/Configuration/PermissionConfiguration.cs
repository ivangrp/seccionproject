﻿using Seccion.Model.IdentityModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Data.Configuration
{
    public class PermissionConfiguration : BaseEntityTypeConfiguration<Permission>
    {
        public PermissionConfiguration()
        {
            ToTable(nameof(Permission));
            HasKey(x => x.PermissionId);
            Property(x => x.PermissionId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasMaxLength(50).IsRequired();
            //base.Configure();
        }
    }
}
