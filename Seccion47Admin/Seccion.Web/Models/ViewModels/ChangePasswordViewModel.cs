﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class ChangePasswordViewModel
	{
        [Display(Name = "Nome Usaurio")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Actual")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 8)]
        [RegularExpression(@"(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z0-9])(?=.*[a-z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[^A-Za-z0-9])(?=.*[^a-zA-Z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[^a-zA-Z])(?=.*[^A-Za-z])(?=.*[!%&@#$*?_~-])
                            |(?=.*\d)(?=.*[^0-9])(?=.*[!%&@#$*?_~-])){8,15}^.*", ErrorMessage = "Pelo menos uma letra maiúscula, Pelo menos uma letra minucula, Pelo menos um dígito, Sem espaços em branco, Pelo menos 1 caractere especial")]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva Contraseña")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Nueva Contraseña")]
        //[Compare("NewPassword", ErrorMessageResourceType = typeof(Languaje), ErrorMessageResourceName = "ValidateConfirmPassw0rd")]
        public string ConfirmPassword { get; set; }
        public string UserId { get; set; }
        public string code { get; set; }
    }
}