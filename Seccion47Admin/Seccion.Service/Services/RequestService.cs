﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using Seccion.Service.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Seccion.Service.Services
{
	public class RequestService: IRequestService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IRequestRepository requestRepository;

		public RequestService(IUnitOfWork unitOfWork, IRequestRepository requestRepository)
		{
			this.unitOfWork = unitOfWork;
			this.requestRepository = requestRepository;
		}
		public async Task<IEnumerable<SP_RequestDto>> GetListRequest(params object[] parameters)
		{
			return await requestRepository.GetListRequest(parameters);
		}
		public async Task<TbRequest> GetRequestById(int id)
		{
			return await requestRepository.Get(x => x.TbRequestId == id);
		}
		public void SaveRequest(TbRequest model)
		{
			requestRepository.Add(model);
		}
		public void UpdateRequest(TbRequest model)
		{
			requestRepository.Update(model);
		}
		public string GetMaxOrderNumber()
		{
			var maxOrder = requestRepository.GetMaxRequest();
			if (maxOrder == null)
				return "SE0000001";


			return "SE" + ((Convert.ToInt64(maxOrder.Substring(2, 7)) + 1)).ToString().PadLeft(7, '0');
		}
		public async Task Save()
		{
			await unitOfWork.Commit();
		}

		
	}
}
