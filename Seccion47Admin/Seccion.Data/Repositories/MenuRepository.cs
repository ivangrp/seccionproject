﻿using Seccion.Data.Infrastructure;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
    public interface IMenuRepository : IRepository<MenuItem>
    {

    }
    public class MenuRepository : RepositoryBase<MenuItem>, IMenuRepository
    {
        public MenuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
