﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Seccion.Web.Common
{
	public class SeparateDecimal
	{
        protected internal static string CleanDecimal(string s)
        {
            return new StringBuilder(s)
                .Replace("$", "")
                .Replace(",", "")
                .Replace(".", ",")
                .ToString();
        }
        protected internal static string CleanDecimalFromDataBase(string s)
        {
            return new StringBuilder(s)
                .Replace(",", ".")
                .ToString();
        }
    }
}