﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User")]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [Display(Name = "Politics")]
        public bool TermsConditions { get; set; }

    }
}