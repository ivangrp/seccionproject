﻿using System.Collections.Generic;

namespace Seccion.Service.Common.Message
{
    public class Notifier : INotifier
    {
        public IList<Message> Messages { get;  set; }

        public Notifier()
        {
            Messages = new List<Message>();
        }
        public void AddMessage(MessageSeverity severity, string title, string text, params object[] format)
        {
            Messages.Add(new Message { Severity = severity, Title= title, TextMessage = string.Format(text, format) });
        }
    }

}