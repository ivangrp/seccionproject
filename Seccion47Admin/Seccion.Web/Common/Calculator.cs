﻿using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Common
{
	public class Calculator
	{
		protected internal static void OperationResult(RequestViewModel model, out decimal Expensesresult, out decimal TotalPay, out decimal CatorcenalMensual)
		{
			try
			{
				var cantidad = Convert.ToDecimal(SeparateDecimal.CleanDecimal(model.Amount));
				var porcentaje = Convert.ToDecimal(SeparateDecimal.CleanDecimal(model.AdministrationExpenses));
				var sumaMeses = Convert.ToInt32(model.PaymentsPeriod) + Convert.ToInt32(model.AdditionalMonths);// parseFloat($('#PaymentsPeriod').val()) + parseFloat($('#AdditionalMonths').val());
				var tasaDescuento = Convert.ToDecimal(SeparateDecimal.CleanDecimal(model.DiscountRate)); //$("#DiscountRate").val();
				var montoCantidad = cantidad;

				var result = Math.Round(montoCantidad * porcentaje / 100,2); // (parseFloat(montoCantidad) * parseFloat(porcentaje)) / 100;
				var resultDescuento = Math.Round(montoCantidad * tasaDescuento / 100,2);//1,500.00
				var resultMeses = resultDescuento * sumaMeses;//16,500.00
				var totalpagar = montoCantidad + resultMeses + result;

				var pago = Convert.ToDecimal(0.00);
				if (model.Location == 1)
				{
					pago = Math.Round(totalpagar / 20,2);
				}
				if (model.Location == 2)
				{
					pago = Math.Round((totalpagar / 20) * 2, 2);
				}

				//if (!isNaN(result))
				//$("#AdministrationExpensesResult").val(result).formatCurrency();
				Expensesresult = result;
				//if (!isNaN(totalpagar) && !isNaN(pago))
				//$("#TotalPay").val(totalpagar).formatCurrency(); $("#AbonoCatorcenalMensual").val(pago).formatCurrency();
				TotalPay = totalpagar;
				CatorcenalMensual = pago;
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
	}
}