﻿


//IdFom: es el id del formulario,
//required: se pasa el mensaje de requerido pro cada vista,
//success. se pasa el mensaje de campo valido
//RulesValidation: se le pasa un bojecto JSon para agregar reglas personalizadas de validacion y se crean en el archivo "VariblesJSONToValidation"
function ValidatorForm(IdForm, required, success, RulesValidation) {
	var $invalidClass = 'brc-danger-tp2';
	var $validClass = 'brc-info-tp2';
	//Personaliza el mensaje para campos requeridos
	jQuery.extend(jQuery.validator.messages, {
		required: required
	});
	var $form = "#" + IdForm;

	$.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $($form).validate({
		ignore: [], // ignore hidden fields
		errorClass: 'form-text-validation form-error text-danger-m2',
		//successClass: 'validation-valid-label',
		//validClass: 'brc-info-tp2',
		//errorElement: 'span',
		highlight: function (element, errorClass) {
			//$(element).removeClass(errorClass);
			var $element = $(element);

			//remove error messages to be inserted again, so that the .fa-exclamation-circle is inserted in `errorPlacement` function
			$element.closest('.form-group').find('.form-text-validation').remove();

			if ($element.is('input[type=checkbox]') || $element.is('input[type=radio]')) {
				return;
			}
			else if ($element.is('.select2')) {
				//var container = $element.siblings('[class*="select2-container"]');
				$element.siblings('[class*="select2-container"]').find('.select2-selection').addClass($invalidClass);
			}
			else if ($element.is('.chosen') || $element.is('select')) {
				//var container = $element.siblings('[class*="chosen-container"]');
				$element.siblings('[class*="chosen-container"]').find('.chosen-choices, .chosen-single').addClass($invalidClass);
			}
			else if ($element.is('select')) {
				//var container = $element.siblings('[class*="chosen-container"]');
				$element.siblings('[class*="chosen-container"]').find('.chosen-choices, .chosen-single').addClass($invalidClass);
			}
			else {
				$element.addClass($invalidClass + ' d-inline-block').removeClass($validClass);
			}
		},
		//unhighlight: function (element, errorClass) {
		//    $(element).removeClass(errorClass);
		//},
		success: function (error, element) {
			//label.addClass("validation-valid-label").text(success)
			//label.addClass('validation-valid-label').text(success); // remove to hide Success message
			var parent = error.parent();
			var $element = $(element);

			$element.removeClass($invalidClass).closest('.form-group').find('.form-text-validation').remove();

			//if ($element.is('input[type=checkbox]') || $element.is('input[type=radio]') || $element.is('input[type=text]') || $element.is('input[type=password]')) {
			//    //return;
			//    $element.addClass($validClass);
			//}
			//else
			if ($element.is('.select2')) {
				//var container = $element.siblings('[class*="select2-container"]');
				$element.siblings('[class*="select2-container"]').find('.select2-selection').removeClass($invalidClass);
			}
			else if ($element.is('.chosen')) {
				//var container = $element.siblings('[class*="chosen-container"]');
				$element.siblings('[class*="chosen-container"]').find('.chosen-choices, .chosen-single').removeClass($invalidClass);
			}

			//else {
			//    $element.addClass($validClass);
			//}

			//append 'ok' mark
			parent.append('<span class="form-text-validation text-success-m1 ml-sm-2"><i class="fa fa-check-circle text-100 mr-1"></i>' + success + ' </span > ');
		},
		rules: RulesValidation === undefined ? {} : CreateRulesValidation(RulesValidation),

		// Different components require proper error label placement
		errorPlacement: function (error, element) {
			error.prepend('<i class="fa fa-times-circle text-danger-m1 text-100 mr-1 ml-2"></i>');
			if (element.is('input[type=checkbox]') || element.is('input[type=radio]') || element.is('input[type=text]') || element.is('input[type=password]')) {
				element.closest('div[class*="form-group"]').append(error);
				//error.insertAfter(element.closest('div[class*="col-"]').append(error));
			}
			else if (element.is('.select2')) {
				//var container = element.siblings('[class*="select2-container"]');
				error.insertAfter(element.siblings('[class*="select2-container"]'));
				container.find('.select2-selection').addClass($invalidClass);
			}
			else if (element.is('.chosen')) {
				//var container = element.siblings('[class*="chosen-container"]');
				error.insertAfter(element.siblings('[class*="chosen-container"]'));
				//container.find('.chosen-choices, .chosen-single').addClass($invalidClass);
			}
			else if (element.is('select')) {
				//var container = element.siblings('[class*="chosen-container"]');
				error.insertAfter(element.siblings('[class*="chosen-container"]'));
				//container.find('.chosen-choices, .chosen-single').addClass($invalidClass);
			}
			else {
				//error.addClass('d-inline-block').insertAfter(element);
				error.insertAfter(element);
			}
			// Styled checkboxes, radios, bootstrap switch
			//if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
			//    if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
			//        error.appendTo(element.parent().parent().parent().parent());
			//    }
			//    else {
			//        error.appendTo(element.parent().parent().parent().parent().parent());
			//    }
			//}

			//    // Unstyled checkboxes, radios
			////else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
			////    error.appendTo(element.parent().parent().parent());
			//    //}
			//// Unstyled checkboxes, radios
			//else if (element.parents().hasClass('form-check')) {
			//    error.appendTo(element.parents('.form-check').parent());
			//    //error.appendTo(element.parent().parent().parent());
			//}

			//    // Input with icons and Select2
			//else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
			//    error.appendTo(element.parent());
			//}

			//    // Inline checkboxes, radios
			//else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
			//    error.appendTo(element.parent().parent());
			//}

			//    // Input group, styled file input
			//else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
			//    error.appendTo(element.parent().parent());
			//}
			////styled file input
			//else if (element.parent().find('div').hasClass('bootstrap-filestyle')) {
			//    error.insertAfter(element.parent().find('div')[0]);
			//}

			//else {
			//    error.insertAfter(element);
			//}
		}

	});


}

//CreateRulesValidation. funcion que ayuda dinamicamente a crear reglas de validacion personalizadas
function CreateRulesValidation(jsonData) {
	var validationMessages = {},
		validationRulesRequired = {},
		validationRulesType = {},
		objectRul = new Object(),
		validationRules;

	$.each(jsonData.rows, function (key, value) {
		$.each(value, function (ikey, ivalue) {

			// generate validation messages
			if (ivalue.ValidationMessage) {
				validationMessages[ivalue.Name] = ivalue.ValidationMessage;
			}

			// generate validation rules

			$.each(ivalue.Datatype, function (keyrule, TypeRule) {

				//validationInputTypes Se encuentra en el archivo VaribleJSONToValidate.js
				if ($.inArray(TypeRule, validationInputTypes) > -1) {
					if (TypeRule === "url" || TypeRule === "email" || TypeRule === "date"
						|| TypeRule === "number" || TypeRule === "digits" || TypeRule === "creditcard"
						|| TypeRule === "customEmail" || TypeRule === "RegularCPFRG"
						|| TypeRule === "RegularCP" || TypeRule === "TelephoneBR"
						|| TypeRule === "minlength" || TypeRule === "MayusculaNumeros" || TypeRule === "check_Select"
						|| TypeRule === "numberNotStartWithZero")
						objectRul[TypeRule] = true;

					if (TypeRule === "maxlength")
						objectRul[TypeRule] = ivalue.maxlength;
					if (TypeRule === "minlength")
						objectRul[TypeRule] = ivalue.minlength;

					validationRulesType[ivalue.Name] = objectRul;
				}
			});
			if (ivalue.IsRequired) {
				validationRulesRequired[ivalue.Name] = {
					'required': true
				};
			} else {
				validationRulesRequired[ivalue.Name] = {
					'required': false
				};
			}
			objectRul = {};

		});
	});
	validationRules = $.extend(true, {}, validationRulesRequired, validationRulesType);
	//console.log(validationRules);
	return validationRules;


}

function ComplementDataTable() {
	$.extend($.fn.dataTable.defaults, {
		autoWidth: false,
		responsive: true,
		dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip><"search-box"r>',
		language: {
			search: '<span>Buscar:</span>',
			searchPlaceholder: 'Inclua um filtro...',
			lengthMenu: '<span>Mostrar:</span> _MENU_',
			info: 'Mostrando del _START_ al _END_ de _TOTAL_ registros',
			emptyTable: 'Nenhuma informação disponível na tabela',
			zeroRecords: 'Não foram encontradas correspondências',
			paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
		}
	});

}

function ComplementDataTableExcel() {
	$.extend($.fn.dataTable.defaults, {
		autoWidth: false,
		responsive: true,
		dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
		language: {
			search: '<span>Buscar:</span>',
			searchPlaceholder: 'Inclua um filtro...',
			lengthMenu: '<span>Mostrar:</span> _MENU_',
			info: 'Mostrando del _START_ al _END_ de _TOTAL_ registros',
			emptyTable: 'Nenhuma informação disponível na tabela',
			zeroRecords: 'Não foram encontradas correspondências',
			paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
		}
	});
}

function NotifierMessage(typeNotofier, title, textMessage) {

	PNotify.defaults.styling = 'material';
	PNotify.defaults.icons = 'material';

	if (typeof window.stackBarTop === 'undefined') {
		window.stackBarTop = {
			'dir1': 'down',
			'dir2': 'right',
			'firstpos1': 64.2,
			'firstpos2': 300,
			'spacing1': 0,
			'push': 'top',
			//'context': document.getElementById('stack-context'),
		};
	}

	var opts = {
		title: '',
		titleTrusted: true,
		text: "",
		textTrusted: true,
		addClass: 'stack-bar-top',
		cornerClass: 'ui-pnotify-sharp',
		shadow: false,
		width: '100%',
		delay: 6000,
		modules: {
			Buttons: {
				sticker: false
			},
			Animate: {
				animate: true,
				inClass: "fadeInRight",
				outClass: "rotateOut"
			}
		},
		stack: window.stackBarTop
	};
	switch (typeNotofier) {
		case 'error':
			opts.title = title;
			opts.text = textMessage;
			opts.type = 'error';
			break;
		case 'info':
			opts.title = title;
			opts.text = textMessage;
			opts.type = 'info';
			break;
		case 'success':
			opts.title = title;
			opts.text = textMessage;
			opts.type = 'success';
			break;
		case 'notice':
			opts.title = title;
			opts.text = textMessage;
			opts.type = 'notice';
			break;
	}
	PNotify.alert(opts);
}

var messageSeverity = {
	success: "success",
	Error: "error",
	Info: "info",
	Warning: "notice"
}

//Formatea de /Date(1245398693390)/ a dd-mes-yyyy HH:mm
function ToJavaScriptDateTime(value) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(value);
	var dt = new Date(parseFloat(results[1]));
	var dd = dt.getDate();
	var mm = dt.getMonth();
	var yyyy = dt.getFullYear();
	var hours = dt.getHours();
	var minutes = dt.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? '0' + hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + "Hrs." + ' ' + ampm;
	var dias = dd < 10 ? '0' + dd : dd;

	var fecha = dias + '-' + GetMonthNameSpanish(mm) + '-' + yyyy + ' ' + strTime;
	return fecha;
}
function GetMonthNameSpanish(monthNumber) {

	switch (monthNumber) {

		case 0:
			return "Jan";
		case 1:
			return "Fev";
		case 2:
			return "Mar";
		case 3:
			return "Abr";
		case 4:
			return "Mai";
		case 5:
			return "Jun";
		case 6:
			return "Jul";
		case 7:
			return "Ago";
		case 8:
			return "Set";
		case 9:
			return "Out";
		case 10:
			return "Nov";
		case 11:
			return "Dez";

	}

}

function GetModalViewPartial(url, Id, catalog, titleModal) {
	$.get(
		url,
		{
			ModelID: Id,
			typeCat: catalog
		},
		function (data) {
			$('.modal-title').html(titleModal);
			$('#ContainerModal').html(data);
			$('#ModalCatalogs').modal('show');
		});
}

//Formate una cantidad $30,000.00 a 30000.00
function TransforMoney(money) {
	var moneyReturn = "";
	var transfor = money.substring(1).split(',');
	for (var i = 0; i < transfor.length; i++) {
		moneyReturn += transfor[i];
	}
	return moneyReturn;
}
function FormatDate(paramDate) {
	if (paramDate !== null) {
		var date = new Date(paramDate);
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return (day < 10 ? "0" + day : day) + "-" + (month < 10 ? "0" + month : month) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
	} else {
		return "";
	}
}

function FormatDateShort(paramDate) {
	if (paramDate !== null) {
		var date = new Date(paramDate);
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return (day < 10 ? "0" + day : day) + "-" + (month < 10 ? "0" + month : month) + "-" + date.getFullYear();
	} else {
		return "";
	}
}

//Formatea de /Date(1245398693390)/ a dd-mes-yyyy HH:mm
function ToJavaScriptDateTime(value) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(value);
	var dt = new Date(parseFloat(results[1]));
	var dd = dt.getDate();
	var mm = dt.getMonth() + 1;
	var yyyy = dt.getFullYear();
	var hours = dt.getHours();
	var minutes = dt.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours == 0 ? '0' + hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	//var strTime = hours + ':' + minutes + ' ' + "Hrs." + ' ' + ampm;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	var dias = dd < 10 ? '0' + dd : dd;

	//var fecha = dias + '-' + GetMonthNameSpanish(mm) + '-' + yyyy + ' ' + strTime;
	var fecha = dias + '-' + (mm < 10 ? '0' + mm : mm) + '-' + yyyy + ' ' + strTime;
	return fecha;
}

//Formatea de /Date(1245398693390)/ a dd-mes-yyyy
function ToJavaScriptDate(value) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(value);
	var dt = new Date(parseFloat(results[1]));
	var dd = dt.getDate();
	var mm = dt.getMonth() + 1;
	var yyyy = dt.getFullYear();
	//var hours = dt.getHours();
	//var minutes = dt.getMinutes();
	//var ampm = hours >= 12 ? 'pm' : 'am';
	//hours = hours % 12;
	//hours = hours == 0 ? '0' + hours : 12; // the hour '0' should be '12'
	//minutes = minutes < 10 ? '0' + minutes : minutes;
	//var strTime = hours + ':' + minutes + ' ' + "Hrs." + ' ' + ampm;
	//var strTime = hours + ':' + minutes + ' ' + ampm;
	var dias = dd < 10 ? '0' + dd : dd;

	//var fecha = dias + '-' + GetMonthNameSpanish(mm) + '-' + yyyy + ' ' + strTime;
	var fecha = dias + '-' + (mm < 10 ? '0' + mm : mm) + '-' + yyyy;
	return fecha;
}
function number_to_price(v) {
	if (v === 0) { return '0,00'; }
	v = v.replace(',', '.');
	v = parseFloat(v);
	v = v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	v = v.split('.').join('*').split(',').join('.').split('*').join(',');
	return v;
}
function RentalCost(data) {

	var arr = data.split(',');
	//subsequently replace all the periods (1.000 separators) with spaces
	arr[0] = arr[0].replace(/[\.]/g, "");
	//join the pieces together with a period if not empty
	if (arr[0] > '' || arr[1] > '') {
		data = arr[0] + '.' + arr[1];
	} else {
		return '';
	}
	data = data.replace(/[^\d.-]/g, ",");
	//data = data.replace(".", ",");
	return data;
}

function stringToBoolean(string) {
	switch (string.toLowerCase().trim()) {
		case "true": case "yes": case "1": return true;
		case "false": case "no": case "0": case null: return false;
		default: return Boolean(string);
	}
}

function toJSONLocal(date) {
	var local = new Date(date);
	local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
	return local.toJSON().slice(0, 10);
}
//Crea un objeto paras los link del menu
function DataMenu(Id, EditLink, DetailLink, Delete) {
	
	return  {
		Id: Id,
		EditLink: EditLink,
		DetailLink: DetailLink,
		Delete: Delete
	};	
}