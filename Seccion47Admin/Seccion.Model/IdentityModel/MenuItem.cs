﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    //[NotMapped]
    public class MenuItem : BaseEntityModel
    {
        public MenuItem()
        {
            Children = new HashSet<MenuItem>();
            Roles = new HashSet<ApplicationRoleMenu>();
        }

  
        public int MenuItemId { get; set; }


        public string Title { get; set; }


        public string Description { get; set; }

        public int? ParentId { get; set; }


        public string Icon { get; set; }


        public string Url { get; set; }

  
        public virtual ICollection<MenuItem> Children { get; set; }

        public virtual MenuItem ParentItem { get; set; }

        public virtual ICollection<ApplicationRoleMenu> Roles { get; set; }

    }
}
