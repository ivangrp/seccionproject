﻿using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public interface IEmployeeRepository : IRepository<TbEmployee>
	{
		Task<IEnumerable<SP_EmployeeDto>> GetEmployeeAll(params object[] parameters);
		Task<IEnumerable<EmployeeDto>> GetEmployeeData();
		Task<EmployeeDto> GetEmployeeByFicha(string numberFicha);
	}
	public class EmployeeRepository : RepositoryBase<TbEmployee>, IEmployeeRepository
	{
		public EmployeeRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
		public async Task<IEnumerable<SP_EmployeeDto>> GetEmployeeAll(params object[] parameters)
		{
			try
			{
				using (var connection = DbContext.Database.Connection)
				{
					connection.Open();
					var command = connection.CreateCommand();
					//Agregar el SP a utilizar
					command.CommandText = "[dbo].[spGetEmployees]";//"[dbo].[SP_Employee_SelectAll]";
					command.Parameters.AddRange(parameters);
					command.CommandType = CommandType.StoredProcedure;
					using (var reader = await command.ExecuteReaderAsync())
					{
						var result = ((IObjectContextAdapter)DbContext).ObjectContext.Translate<SP_EmployeeDto>(reader).ToList();
						return result;
					}
				}
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
		public async Task<IEnumerable<EmployeeDto>> GetEmployeeData()
		{
			try
			{
				using (var context = DbContext)
				{
					var resultDto = await context.Employee.Select(x => new EmployeeDto
					{
						EmployeeId = x.TbEmployeeId,
						NameEmployee  = x.Name + " " + x.FirstName + " " + x.LastName + " (" + x.NumberFicha + ")",
						NumberFicha = x.NumberFicha,
						ContractualSituation = x.ContractualSituation,
						WorkLocation = x.WorkLocation
					}).ToListAsync();

					return resultDto;
				}
			}
			catch (Exception ex)
			{

				throw ex;
			}

		}
		public async Task<EmployeeDto> GetEmployeeByFicha(string numberFicha)
		{
			try
			{

					var resultDto = await DbContext.Employee.Where(x => x.NumberFicha == numberFicha).Select(x => new EmployeeDto
					{
						EmployeeId = x.TbEmployeeId,
						NameEmployee =  x.Name,
						FirstName = x.FirstName,
						LastName = x.LastName

					}).FirstOrDefaultAsync();

					return resultDto;

			}
			catch (Exception ex)
			{

				throw ex;
			}

		}
	}
}
