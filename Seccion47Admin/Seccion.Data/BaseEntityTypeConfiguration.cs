﻿using Seccion.Model;
using System.Data.Entity.ModelConfiguration;

namespace Seccion.Data
{
    public abstract class BaseEntityTypeConfiguration<T> : EntityTypeConfiguration<T> where T : BaseEntityModel
    {
        public BaseEntityTypeConfiguration()
        {
            Property(x => x.DateInsert).IsOptional();
            Property(x => x.DateUpdate).IsOptional();
            Property(x => x.UserInsert).HasMaxLength(50);
            Property(x => x.UserUpdate).HasMaxLength(50);
        }
    }
}
