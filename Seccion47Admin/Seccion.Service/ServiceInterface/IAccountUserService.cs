﻿using Microsoft.AspNet.Identity;
using Seccion.Data.Enum;
using Seccion.Model.IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Service.ServiceInterface
{
	public interface IAccountUserService
	{
		Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass);
		void AccountUserUpdate(ApplicationUser user);
		Task<IEnumerable<ApplicationUser>> GetUserList();
		Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser);
		Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword);
		Task<string> EmailConfirmationToken(ApplicationUser appUser);
		Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail);
		Task<ApplicationUser> GetUserById(int id);
		Task<SignInStatusUser> SignInUser(string userName, string password);
		List<Tuple<int, string>> GetRoles();
		Task<IdentityResult> AddRole(int userId, string role);
		Task<string> GetRoleById(int roleId);
		Task<IList<string>> GetRoleByUserId(int userId);
		Task<IdentityResult> DeleteRoleByUserId(int userId, string role);
		void SaveUser();
	}
}
