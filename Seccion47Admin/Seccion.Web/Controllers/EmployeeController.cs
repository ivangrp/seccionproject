﻿using AutoMapper;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Common;
using Seccion.Web.Models.Dtos;
using Seccion.Web.Models.GridListModel;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService employeeService;
        private readonly INotifier notifier;

        public EmployeeController(IEmployeeService employeeService, INotifier notifier)
        {
            this.employeeService = employeeService;
            this.notifier = notifier;
        }
        // GET: View Listado Empleado
        public async Task<ActionResult> ListEmployee()
        {
            
            return View();
        }
        public async Task<JsonResult> GetListEmployee(DatatableServerSideModel param)
        {
            SqlParameter[] parameters = new SqlParameter[5];
            parameters[0] = new SqlParameter("@DisplayLength", param.length);
            parameters[1] = new SqlParameter("@DisplayStart", param.start);
            parameters[2] = new SqlParameter("@SortCol", param.order[0].column);
            parameters[3] = new SqlParameter("@SortDir", param.order[0].dir);
            parameters[4] = new SqlParameter("@Search", param.search.value);

            //parameters[0] = new SqlParameter("@EmployeeID", SqlDbType.Int);
            //parameters[0].Value = DBNull.Value;
            //parameters[1] = new SqlParameter("@Ficha", "");
            //parameters[2] = new SqlParameter("@Name", "");
            //parameters[3] = new SqlParameter("@FirstName", "");
            //parameters[4] = new SqlParameter("@LastName", "");
            //parameters[5] = new SqlParameter("@FirstRow", param.start);
            //parameters[6] = new SqlParameter("@LastRow", param.length);
            //parameters[7] = new SqlParameter("@SortColumn", "");
            //parameters[8] = new SqlParameter("@SortOrder", "");
            //var data = _vehicularRequestService.GetActiveRequest("[dbo].[SP_GetVehiculeReporchase]", new SqlParameter("@UserTypeID", SqlDbType.UniqueIdentifier) { Value = userId }).ToList();
            var employeeList = await employeeService.GetEmployeeAll(parameters);
            var model = Mapper.Map<IEnumerable<SP_EmployeeDto>, IEnumerable<ListEmployeeViewModel>>(employeeList);
            return Json(new
            {
                draw = Convert.ToInt32(param.draw),
                recordsTotal = model.Last().TotalEmployees,
                recordsFiltered = model.Last().TotalEmployees,
                data = model
            }, JsonRequestBehavior.AllowGet);
            //return Json(model);
        }
        
        // GET: View Crear Empleado
        public async Task<ActionResult> CreateEmployee()
        {
            ViewBag.ContractualSituation = ListsDrops.GetListContractualSituation();
            ViewBag.WorkLocation = ListsDrops.GetListWorkLocation();
            ViewBag.Region = ListsDrops.GetListRegion();
            ViewBag.Level = ListsDrops.GetListLevel();
            var stateList = await employeeService.GetStatesAll();
            ViewBag.TbStatesId = new SelectList(stateList.OrderBy(x => x.TbStatesId), "TbStatesId", "NameSate");

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateEmployee(EmployeeVIewModel modelEmployee)
        {
            try
            {
                var employee = Mapper.Map<EmployeeVIewModel, TbEmployee>(modelEmployee);

                employee.UserInsert = User.Identity.Name;
                employee.DateInsert = DateTime.Now;
                employee.ActiveEmployee = true;
                employeeService.SaveEmployeeData(employee);
                await employeeService.Save();
                notifier.Success("Éxito!!", "Los datos del Empleado se registraron correctamente.");
                return RedirectToAction("ListEmployee", "Employee");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "ListEmployee", "Employee"));
            }
            
        }

        // GET: View Editar Datos Empleado
        public async Task<ActionResult> EditDataEmployee(int? id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                EmployeeVIewModel model= null;
                var resultEmployee = await employeeService.GetEmployeeById(id);
                if (resultEmployee != null)
                {
                    model = Mapper.Map<TbEmployee, EmployeeVIewModel>(resultEmployee);
                    var dropsDto = new DropsDto
                    {
                        State = model.TbStatesId,
                        City = model.TbMunicipalitiesId,
                        SituacionContractual = model.ContractualSituation,
                        WorkLocation = model.WorkLocation,
                        Region = model.Region,
                        Level = model.Level
                    };
                    await LoadDrops(dropsDto);
                    
                }
                return View(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDataEmployee(EmployeeVIewModel modelEmployee)
        {
            try
            {
                TbEmployee updateEmployee = await employeeService.GetEmployeeById(modelEmployee.TbEmployeeId);
                if(updateEmployee != null)
                {
                    updateEmployee = Mapper.Map(modelEmployee, updateEmployee);
                    updateEmployee.UserUpdate = User.Identity.Name;
                    updateEmployee.DateUpdate = DateTime.Now;
                    employeeService.UpdateEmployeeData(updateEmployee);
                    await employeeService.Save();
                    notifier.Success("Éxito!!", "Los datos del Empleado se Actualizaron correctamente.");
                    return RedirectToAction("ListEmployee", "Employee");
                }

                return View("Error", new HandleErrorInfo( new Exception("A ocurrido un Error, verifica nuevamente"), "ListEmployee", "Employee"));
            }
            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "ListEmployee", "Employee"));
            }
            
        }

        [HttpGet]
        public async Task<ActionResult> DetailEmployee(int id)
        {
            TbEmployee deatilEmployee = await employeeService.GetEmployeeById(id);

            var model = Mapper.Map<TbEmployee, EmployeeVIewModel>(deatilEmployee);
            return View(model);
        }
        public async Task<JsonResult> GetMuniciplaities(int id)
        {
            var resultList = await employeeService.GetMunicipalitiesAll(id);
            return Json(resultList.ToList(), JsonRequestBehavior.AllowGet);
        }

        private async Task LoadDrops(DropsDto dropsDto= null)
        {
            var stateList = await employeeService.GetStatesAll();
            if (dropsDto == null)
            {
                ViewBag.ContractualSituation = ListsDrops.GetListContractualSituation();
                ViewBag.WorkLocation = ListsDrops.GetListWorkLocation();
                ViewBag.Region = ListsDrops.GetListRegion();
                ViewBag.Level = ListsDrops.GetListLevel();
                ViewBag.TbStatesId = new SelectList(stateList.OrderBy(x => x.TbStatesId), "TbStatesId", "NameSate");
            }
            else
            {
                ViewBag.ContractualSituation = ListsDrops.GetListContractualSituation(dropsDto.SituacionContractual);
                ViewBag.WorkLocation = ListsDrops.GetListWorkLocation(dropsDto.WorkLocation);
                ViewBag.Region = ListsDrops.GetListRegion(dropsDto.Region);
                ViewBag.Level = ListsDrops.GetListLevel(dropsDto.Level);
                ViewBag.TbStatesId = new SelectList(stateList.OrderBy(x => x.TbStatesId), "TbStatesId", "NameSate", dropsDto.State);
                ViewBag.TbMunicipalitiesId = new SelectList(await employeeService.GetMunicipalitiesAll(dropsDto.State), "TbMunicipalitiesId", "NameMunicipalities", dropsDto.City);
            }
           
        }
    }
}
