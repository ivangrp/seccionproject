﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.ResposeDto
{
	public class SP_RequestDto
	{
		public Int64 RowNum { get; set; }
		public int TotalRequest { get; set; }
		public int TbRequestId { get; set; }
		public int TbEmployeeId { get; set; }
		public string RequestNumber { get; set; }
		public DateTime DateCreateRequest { get; set; }
		public string Solicitante { get; set; }
		public decimal Amount { get; set; }
		public string PaymentsPeriod { get; set; }
		public decimal DiscountRate { get; set; }
		public decimal AdministrationExpenses { get; set; }
		public decimal AdministrationExpensesResult { get; set; }
		public string AdditionalMonths { get; set; }
		public decimal AbonoCatorcenalMensual { get; set; }
		public decimal TotalPay { get; set; }
		public string AvalName { get; set; }
		public decimal DebitAval { get; set; }

	}
}
