﻿using Seccion.Model.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace Seccion.Data.Configuration
{
    public class TbEmployeeConfiguration : BaseEntityTypeConfiguration<TbEmployee>
    {
        public TbEmployeeConfiguration()
        {

            ToTable(nameof(TbEmployee));
            HasKey(t => t.TbEmployeeId);
            Property(x => x.TbEmployeeId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            HasRequired(x => x.States).WithMany(x => x.Employee).HasForeignKey(x => x.TbStatesId).WillCascadeOnDelete(false);
            HasRequired(x => x.Municipalities).WithMany(x => x.Employee).HasForeignKey(x => x.TbMunicipalitiesId).WillCascadeOnDelete(false);
            Property(x => x.NumberFicha).IsRequired().HasMaxLength(20)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("NumberFichaIndex") { IsUnique = true })); ;
            Property(x => x.Name).HasMaxLength(50).IsRequired();
            Property(x => x.FirstName).HasMaxLength(50).IsRequired();
            Property(x => x.LastName).HasMaxLength(50).IsRequired();
            Property(x => x.RFC).HasMaxLength(20).IsRequired();
            Property(x => x.BirthDate).IsOptional();
            Property(x => x.DepartmentKey).HasMaxLength(50);
            Property(x => x.AsisteKey).HasMaxLength(100);
            Property(x => x.Address).HasMaxLength(200).IsRequired();
            Property(x => x.State).HasMaxLength(50);
            Property(x => x.City).HasMaxLength(50);
            Property(x => x.PhoneHouse).HasMaxLength(50);
            Property(x => x.CelularPhone).HasMaxLength(50);
            Property(x => x.Email).HasMaxLength(50);
            Property(x => x.Bank).HasMaxLength(50);
            Property(x => x.BranchOffice).HasMaxLength(50);
            Property(x => x.AccountNumber).HasMaxLength(50);
            Property(x => x.ClaveInterbancaria).HasMaxLength(50);
            Property(x => x.WorkLocation).HasMaxLength(50);
            Property(x => x.Rol).HasMaxLength(50);
            Property(x => x.Region).HasMaxLength(50);
            Property(x => x.ContractualSituation).HasMaxLength(50);
            Property(x => x.Comments).HasMaxLength(400);

            //base.Configure();
        }
    }
}
