﻿using Seccion.Data.Infrastructure;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public interface IMunicipalitiesRepository : IRepository<TbMunicipalities>
	{

	}
	public class MunicipalitiesRepository : RepositoryBase<TbMunicipalities>, IMunicipalitiesRepository
	{
		public MunicipalitiesRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
	}
}
