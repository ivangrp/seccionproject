﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartup(typeof(Seccion.Data.Infrastructure.StartupOwin))]
namespace Seccion.Web
{
    public class Startup
    {
        public  void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            //var configuration = new StartupOwin();
            Data.Infrastructure.StartupOwin.ConfigureAuth(app);
        }
    }
}
