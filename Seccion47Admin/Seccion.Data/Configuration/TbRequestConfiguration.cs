﻿using Seccion.Model.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seccion.Data.Configuration
{
    public class TbRequestConfiguration : BaseEntityTypeConfiguration<TbRequest>
    {
        public TbRequestConfiguration()
        {
            ToTable(nameof(TbRequest));
            HasKey(t => t.TbRequestId);
            Property(x => x.TbRequestId).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            HasRequired(x => x.Employee).WithMany(x => x.Request).HasForeignKey(x => x.TbEmployeeId).WillCascadeOnDelete(false);
            Property(x => x.RequestNumber).HasMaxLength(10);
            Property(x => x.DateCreateRequest).IsRequired();
            Property(x => x.Amount).HasPrecision(18, 2);
            Property(x => x.PaymentsPeriod).HasMaxLength(20);
            Property(x => x.DiscountRate).HasPrecision(18, 2);
            Property(x => x.AdministrationExpenses).HasPrecision(18, 2);
            Property(x => x.AdministrationExpensesResult).HasPrecision(18, 2);
            Property(x => x.AdditionalMonths).HasMaxLength(20);
            Property(x => x.TotalPay).HasPrecision(18, 2);
            Property(x => x.AbonoCatorcenalMensual).HasPrecision(18, 2);
            Property(x => x.CatorcenaLiquidate).HasMaxLength(50);
            Property(x => x.BelongingSection).HasMaxLength(50);
            Property(x => x.AvalName).HasMaxLength(100);
            Property(x => x.AvalKey).HasMaxLength(20);
            Property(x => x.DebitAval).HasPrecision(18, 2);
            Property(x => x.Comments).HasMaxLength(400);
            //base.Configure();
        }
    }
}
