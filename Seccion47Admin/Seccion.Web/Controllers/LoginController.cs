﻿using Microsoft.Owin.Security;
using Seccion.Data.Enum;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
	public class LoginController : Controller
    {
        private readonly IAccountUserService _accountUserService;

        public LoginController(IAccountUserService accountUserService)
        {
            _accountUserService = accountUserService;
        }

        // GET: Login
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult LoginOn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public async Task<ActionResult> LoginOn(LoginViewModel model)
        {
            try
            { 
                var user = await _accountUserService.GetUserByIdForLogin(model.Usuario);

                //Validar Email
                //var user = await _accountUserService.EmailConfirmationToken(model.Usuario);
                var validUser = await _accountUserService.SignInUser(model.Usuario, model.Password);
                if(validUser == SignInStatusUser.Success)
                {
                    var userIdentity = await _accountUserService.CreateUserIdentity(user);
                    HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, userIdentity);
                }

                //return RedirectToAction("Index", "Home");
                return Json("/Home/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize]
        public ActionResult LogOff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            var ctx = Request.GetOwinContext();
            ctx.Response.Cookies.Delete("__RequestVerificationToken");
            AuthenticationManager.SignOut();
            HttpContext.User = null;
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction("LoginOn", "Login");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}