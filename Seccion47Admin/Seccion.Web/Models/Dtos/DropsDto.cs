﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.Dtos
{
	public class DropsDto
	{
		public int State { get; set; }
		public int City { get; set; }
		public string SituacionContractual { get; set; }
		public string WorkLocation { get; set; }
		public string Region { get; set; }
		public int Level { get; set; }
	}
}