﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Infrastructure
{
    /// <summary>
    /// Así que vamos a crear una interfaz de fábrica responsable de inicializar las instancias de esta clase
    /// </summary>
    public interface IDbFactory : IDisposable
    {
        StoreEntities Init();
    }
}
